#!/usr/bin/python
# -*- coding: utf-8 -*-

"""update_collection.py - Arma 3 Dedicated Server helper
"""

__version__ = '0.1'
__author__ = 'Taneli Kaivola'
__software__ = 'Arma 3 Modmanager v{}'.format(__version__)

import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Downloading
from functools import lru_cache
import operator
import requests
try:
    import requests_cache
    from datetime import timedelta
    expire_after = timedelta(days=1)
    requests_cache.install_cache("cache/steam_cache", expire_after=expire_after)
    requests_cache.remove_expired_responses()
except:
    logger.exception('requests_cache initialization failed')
finally:
    requests_sess = requests.Session()

# Parsing
import re
from lxml import html

# Graphing
import networkx

# Generating
from jinja2 import Environment, FileSystemLoader, select_autoescape
env = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


@lru_cache(maxsize=5000, typed=False)
def requests_get(url):
    return requests_sess.get(url)


class WorkshopItem(object):
    baseurl = 'http://steamcommunity.com/sharedfiles/filedetails/?id={}'
    baseurlmatch = re.compile(r'^https?://steamcommunity.com/(?:sharedfiles|workshop)/filedetails/\?id=(\d+)$')
    #

    def __init__(self, uid):
        super().__init__()
        self._uid = uid
        self._tree = None
        self._url = None
    #

    @classmethod
    def from_url(cls, url, force_fetch=True):
        m = WorkshopItem.baseurlmatch.match(url)
        if m is None:
            raise RuntimeError("url not matched: {}".format(url))
            return None
        uid = m.group(1)
        obj = cls(uid)
#        obj._url = url
        if force_fetch:
            page = requests_get(obj.url)
            obj._tree = html.fromstring(page.content)
        return obj
    #

    def __eq__(self, other):
        return self._uid == other._uid
    #

    def __hash__(self):
        return int(self._uid)
    #

    @property
    def uid(self):
        return self._uid
    #

    @property
    def url(self):
        if self._url is not None:
            return self._url
        return WorkshopItem.baseurl.format(self._uid)
    #

    @property
    def parsed(self):
        if self._tree is not None:
            return self._tree
        else:
            page = requests_get(self.url)
            self._tree = html.fromstring(page.content)
            return self._tree

    @property
    def data_type(self):
        try:
            item = self.parsed.xpath('//*[@class="workshopTags"]/*[@class="workshopTagsTitle" and starts-with(., "Data Type:")]/following-sibling::a/text()')[0]
            return item
        except Exception as e:
            pass


class Collection(WorkshopItem):
    def __init__(self, uid):
        super().__init__(uid)

    def linked(self):
        items = self.parsed.xpath('//*[@class="collectionChildren"]/div[contains(@class, "workshopBrowseRow")]/div[contains(@class, "workshopItem")]/a')
        return set([item.get('href') for item in items])

    def mods(self):
        items = self.parsed.xpath('//*[@class="collectionChildren"]//div[contains(@class, "collectionItemDetails")]/a')
        return set(item.get('href') for item in items)

    @property
    def title(self):
        try:
            item = self.parsed.xpath('//div[@id="mainContentsCollectionTop"]//div[@class="workshopItemTitle"]/text()')[0]
        except Exception as e:
            return self._uid
        return item


class Mod(WorkshopItem):
    def __init__(self, uid):
        super().__init__(uid)
    #

    def depends(self):
        items = self.parsed.xpath('//*[@id="RequiredItems"]/a')
        return set([item.get('href') for item in items])

    @property
    def title(self):
        try:
            item = self.parsed.xpath('//div[@id="mainContents"]//div[@class="workshopItemTitle"]/text()')[0]
        except Exception as e:
            return self._uid
        return item


WorkshopItems = {}


def WSI(url, instantiation):
    wsi = WorkshopItem(url)
    uid = wsi.uid
    if uid not in WorkshopItems:
        instance = WorkshopItems[uid] = instantiation()
        return instance
    else:
        return WorkshopItems[uid]


class Resolver(object):
    def __init__(self, initial_collection=None):
        # urls
        self.visited_collections = set()
        self.remaining_collections = set()
        self.visited_mods = set()
        self.remaining_mods = set()
        # objects
        self.collections = set()
        self.mods = set()
        self.graph = networkx.DiGraph()
        self._initial_collection = initial_collection
        if initial_collection:
            self.add_collection(initial_collection)
            WSI(initial_collection, lambda: Collection.from_url(initial_collection, force_fetch=True))

    def add_edge(self, a, b):
        self.graph.add_edge(a, b)

    def add_collection(self, url):
        if (url not in self.visited_collections) and (url not in self.remaining_collections):
            self.remaining_collections.add(url)

    def add_mod(self, url):
        if (url not in self.visited_mods) and (url not in self.remaining_mods):
            self.remaining_mods.add(url)

    def step(self):
        if self.remaining_collections:
            url = self.remaining_collections.pop()
            self.visited_collections.add(url)
            collection = WSI(url, lambda: Collection.from_url(url))
            self.collections.add(collection)
            for linked in collection.linked():
                self.add_collection(linked)
                self.add_edge(collection, WSI(linked, lambda: Collection.from_url(linked)))
            for mod in collection.mods():
                self.add_mod(mod)
                self.add_edge(collection, WSI(mod, lambda: Mod.from_url(mod)))
            return collection
        elif self.remaining_mods:
            url = self.remaining_mods.pop()
            self.visited_mods.add(url)
            mod = WSI(url, lambda: Mod.from_url(url))
            self.mods.add(mod)
            for mod2 in mod.depends():
                self.add_mod(mod2)
                self.add_edge(mod, WSI(mod2, lambda: Mod.from_url(mod2)))
            return mod

    def resolve(self):
        while self.remaining_collections or self.remaining_mods:
            self.step()

    @property
    def ordered_deps(self):
        G = networkx.DiGraph(self.graph)
        found = set()
        while G.nodes():
            leaves = sorted((k for k, v in G.out_degree() if v == 0), key=operator.attrgetter('title'))
            for leaf in leaves:
                G.remove_node(leaf)
                if leaf not in found:
                    found.add(leaf)
                    if isinstance(leaf, Mod):
                        yield leaf

    @property
    def config_mods(self):
        return ("m/{}".format(mod._uid) for mod in self.ordered_deps if mod.data_type != "Scenario")

    @property
    def scenario_list(self):
        return ["{}".format(mod._uid) for mod in self.ordered_deps if mod.data_type == "Scenario"]


def main(url):
    try:
        requests_cache.get_cache().delete(requests_cache.get_cache()._url_to_key(url))
    except Exception as e:
        logger.exception("Couldn't clear cache for source collection")

    r = Resolver(url)
    r.resolve()

    env.get_template("list.html").stream(resolver=r).dump('/web/list.html')

    template = env.get_template("mods.html")
    template.stream(resolver=r, excluded_types=['Scenario']).dump('/web/mods.html')
    template.stream(resolver=r, excluded_types=[]).dump('/web/mods_server.html')

    # preset
    env.get_template("preset.html").stream(resolver=r, preset="Kirvesmurhaajat", excluded_types=['Scenario', None]).dump('/web/preset.html')

    # server configs
    env.get_template("update_mpmissions.sh").stream(resolver=r).dump('/server/update_mpmissions.sh')
    env.get_template("mods.txt").stream(resolver=r).dump('/server/mods.txt')


if __name__ == '__main__':
    collection_url = 'https://steamcommunity.com/sharedfiles/filedetails/?id=1413701042'
    collection_url = WorkshopItem("1413701042").url
    main(collection_url)
