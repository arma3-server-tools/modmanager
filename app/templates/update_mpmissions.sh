#!/bin/sh

cd /arma3/mpmissions/
rm *.pbo
for N in {{ resolver.scenario_list|join(' ') }}; do
M="$(ls /mods/steamapps/workshop/content/107410/$N/*_legacy.bin 2>/dev/null | perl -nle 'print $1 if /(\d+)_legacy\.bin/')"
if [ -n "$M" ]; then
cp ~/Steam/userdata/*/ugc/*/$M/*.pbo .;
else
echo "$N doesn't have _legacy.bin"
fi
done
