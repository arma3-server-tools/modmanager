#!/bin/sh
mkdir -p web server cache
docker build -t modmanager . && docker run -it -v `pwd -P`/web/:/web/ -v `pwd -P`/server/:/server/ -v `pwd -P`/cache/:/usr/src/app/cache/ modmanager
